package com.social.resonance.bean;

import com.social.resonance.utils.ADFUtils;

import com.social.resonance.utils.JSFUtils;
import com.social.resonance.view.LoginViewRowImpl;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;

import javax.security.auth.Subject;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import weblogic.security.URLCallbackHandler;
import weblogic.security.services.Authentication;

import weblogic.servlet.security.ServletAuthentication;

public class LoginBean {
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private Integer userId;
    private String email;
    private Integer category;


    public LoginBean() {
        super();
    }

    public String doLogin() throws LoginException {

        String un = userName;
        byte[] pw = password.getBytes();
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request =
            (HttpServletRequest)ctx.getExternalContext().getRequest();
        Subject mySubject;
        try {
            mySubject =
                    new Subject(); //.login(new URLCallbackHandler(un, pw));
            ServletAuthentication.runAs(mySubject, request);
            ServletAuthentication.generateNewSessionID(request);
            String loginUrl =
                "/adfAuthentication?success_url=/faces/home.jspx";
            HttpServletResponse response =
                (HttpServletResponse)ctx.getExternalContext().getResponse();
            RequestDispatcher dispatcher =
                request.getRequestDispatcher(loginUrl);
            dispatcher.forward(request, response);
            //        } catch (FailedLoginException e) {
            //            FacesMessage msg =
            //                new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid username or password",
            //                                 "Invalid username or password");
            //            ctx.addMessage(null, msg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }


    public void userNameValidator(FacesContext facesContext,
                                  UIComponent uIComponent, Object object) {
        Boolean userExists =
            (Boolean)ADFUtils.getBindingContainer().getOperationBinding("validateUser").execute();
        if (userExists) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                                                          "User already exists",
                                                          null));
        }
    }

    public void fetchUser(ActionEvent actionEvent) {
        LoginViewRowImpl login =
            (LoginViewRowImpl)ADFUtils.getBindingContainer().getOperationBinding("fetchUser").execute();
        if(null!=login){
            this.setCategory(login.getCategory());
            this.setEmail(login.getEmail());
            this.setFirstName(login.getFirstName());
            this.setLastName(login.getLastName());
            this.setUserId(login.getUserId());
            this.setUserName(login.getUserName());
        }
        else{
            JSFUtils.addFacesErrorMessage("USer not authenticated");
        }

    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public String logout() {
        this.userName = null;
        this.password = null;
        return null;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getCategory() {
        return category;
    }
}
